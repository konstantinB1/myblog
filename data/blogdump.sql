-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_auth`
--

DROP TABLE IF EXISTS `admin_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_auth` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(64) NOT NULL,
  `admin_password` varchar(64) NOT NULL,
  `admin_hash` varchar(64) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  `admin_created` date DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_auth`
--

LOCK TABLES `admin_auth` WRITE;
/*!40000 ALTER TABLE `admin_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_post_id` int(11) NOT NULL,
  `comment_email` varchar(100) NOT NULL,
  `comment_name` varchar(64) NOT NULL,
  `comment_body` varchar(355) NOT NULL,
  `comment_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (74,1,'','','rqrqwrqwr','0000-00-00 00:00:00'),(75,1,'','','rwqwrqwrqwr','0000-00-00 00:00:00'),(76,1,'','','y4yeryeryery','0000-00-00 00:00:00'),(77,1,'','Micko Vracarac','Jedite kurac napred zvezda!','0000-00-00 00:00:00'),(83,2,'','Miroslav J','I think i need to pee my pants really soon!!!','0000-00-00 00:00:00'),(84,2,'','Verica Rakocevic','Stvarno neverovatno ja sam odusevljena ovim cudesnim postom LOL!','0000-00-00 00:00:00'),(103,3,'','Supaplex','Sjajno divno divno!','2017-12-25 13:04:34'),(104,3,'','Supaplex','Sjajno divno divno!','2017-12-25 13:04:38'),(105,3,'','qwrqwrqwr','qwrqwrqwrqwrq','2017-12-25 13:13:00'),(106,3,'','Pavlaka','Milos bojanic legenda!','2017-12-25 13:52:29'),(107,3,'','Pavlaka','Milos bojanic legenda!','2017-12-25 13:53:07'),(108,3,'','','qwrqwrqwrqwrqr','2017-12-25 13:53:29'),(109,3,'','','tqtqwtqwtqwtqwt','2017-12-25 13:54:03'),(110,3,'','','qwtqwtqwtqwtqwt','2017-12-25 13:54:28'),(111,3,'','','qwtqwtqwtqwtqwtewtewt','2017-12-25 13:54:40'),(112,3,'','','tqwtqwtqwtqw','2017-12-25 13:54:43'),(113,3,'','','qwtqwtqwtqwtqwtq','2017-12-25 13:55:31'),(114,3,'','Fucking nigger','Yess you  fucking nigger.......','2017-12-25 18:26:12'),(115,18,'','Miroslav','Jedite bre svi govna!','2017-12-26 16:17:39'),(116,18,'','Miroslav','Jedite bre svi govna!','2017-12-26 16:17:42'),(117,18,'','Racist Cunt','Hello world!','2017-12-26 16:19:33'),(118,18,'','qwttqwt','eryerurturturturur','2017-12-26 16:20:07'),(119,18,'','Racist Cunt22','Hello...........','2017-12-26 16:27:35'),(120,18,'','Racist Cunt22222','Hello world!','2017-12-26 16:28:22'),(121,18,'','Racist Cunt22222','Hello world!','2017-12-26 16:28:30'),(122,18,'','qwrqwrqwrqrw','qrqwrqwrqwr','2017-12-26 16:29:19'),(123,18,'','qwrqwrqwrqrw','qrqwrqwrqwr','2017-12-26 16:29:30'),(124,18,'','Racist Cunt','qwrqwrqwrqwr','2017-12-26 16:29:43'),(125,18,'','ewtwtwet','qwrqwrqwrqwrqwr','2017-12-26 16:29:52'),(126,18,'','eewtwetwet','qtqwtqwtwetwetwet','2017-12-26 16:30:34'),(127,18,'','eewtwetwet','qtqwtqwtwetwetwet','2017-12-26 16:30:49'),(128,18,'','tewtewtwet','ewtwetwetwet','2017-12-26 16:30:57'),(129,18,'','ewtwetwetwet','tewtwetwetwetew','2017-12-26 16:31:12'),(130,18,'','ewtwetwetwet','ewtwetwet','2017-12-26 16:31:28'),(131,18,'','ewtwetwetw','etwetwetwet','2017-12-26 16:31:33'),(132,18,'','etwetwetwe','twetwetwetwe','2017-12-26 16:31:51'),(133,18,'','twetwetwet','ewtwetwetew','2017-12-26 16:32:04');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(64) NOT NULL,
  `post_content` varchar(15000) NOT NULL,
  `post_attachments` varchar(25000) NOT NULL,
  `post_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `post_desc` varchar(255) NOT NULL,
  `post_allow_comments` tinyint(1) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (18,'My blog website','												Hello!\r\n\r\nMy name is Konstantin, and this is a little humble website i made using Zend Framework 3, and Javascript (jQuery). \r\n\r\nI tried to revolve around Ajax as much as possible. Even made a small library for fetching post inputs.\r\n\r\nThe site is still not done. I have to patch numerous holes, and add some page access moderation.\r\n																','','2017-12-26 18:06:07','Hello, my name is Konstantin and this is my introduction to this website.',1),(19,'wqrqwrqwrqwr','				post_contentpost_contentpost_contentpost_contentpost_contentpost_contentpost_contentpost_contentpost_contentpost_contentpost_contentpetwtwtost_contentpost_contentpost_content	111111111111111111							','','2017-12-26 17:26:52','Sta se radi na estradi pitate se vi e pa koga bole kuac bue!',1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-26 18:22:41
