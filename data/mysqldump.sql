-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(64) NOT NULL,
  `admin_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_status` varchar(16) NOT NULL,
  `admin_resetpwd_token` varchar(64) NOT NULL,
  `admin_password` varchar(128) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'konstantinbulatovic1@gmail.com','2018-01-02 15:11:16','','','$2y$10$9ga8FgdD49Bz2bS3kH.IZONZ/f.7vbaFOZxVSBQSM1d2pwz/7uEJW');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_post_id` int(11) NOT NULL,
  `comment_email` varchar(100) NOT NULL,
  `comment_name` varchar(64) NOT NULL,
  `comment_body` varchar(355) NOT NULL,
  `comment_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,37,'','Pozdrav za debile','cao cao cao cao!','2018-01-04 15:53:00'),(2,37,'','rqwrqwrqwr','		qwrqwrqwrqwrqr','2018-01-04 15:55:44'),(3,36,'','qwrqwrqwrq','		wrqwrqwrqwrqwrqwr','2018-01-04 16:18:49'),(4,37,'','John dillinger','Fuck niggers!		','2018-01-04 16:24:43'),(5,37,'','ewtwetwet','		wtwetwetwete','2018-01-04 16:30:50'),(6,37,'','Nicholas cage ','Yes yes yes!		','2018-01-04 16:32:57'),(7,37,'','Mr Proper','I really like what you wrote here, so i have to aplly to the fact that you are a monkey and don\'t know shit about this. \nFor sure it would be much better if u were a fucking mongoloid and so far so far la la ...','2018-01-04 17:44:38'),(8,37,'','John ','I like it alot and i have to say i dont like much stuff like that ok...','2018-01-04 21:42:28'),(9,37,'','Bill','Great stuff ! +1','2018-01-04 21:43:32'),(10,37,'','qrwqwrqw','rqwrqwrqwrqwr','2018-01-04 21:45:01'),(11,37,'','Milance','sjajno care sjajno!','2018-01-04 22:09:40'),(12,37,'','qwrqwrqwrqwrqwrq','qwrqwrqwrqwr','2018-01-04 22:09:59'),(13,37,'','qwtqwtqwtqwt','qqwtqwtqwtqwtqwt','2018-01-04 22:10:20'),(14,37,'','qwrqwrqwrqwrqwr','rqwrqwrqwrqrqwrqwr','2018-01-04 22:10:31'),(15,37,'','wetwetwetwetwet','tewtwetwetwetwetwet','2018-01-04 22:10:50'),(16,37,'','qwrqwrqwrqwr','qwrqwrqwrqwrqwrq','2018-01-04 22:11:37'),(17,37,'','Leposava','Jedite svi govna eto vam!','2018-01-04 22:13:26'),(18,37,'','John connor','Hello world','2018-01-04 23:54:23'),(19,37,'','Nikola ','Super ste stvarno hehe','2018-01-05 03:08:38'),(20,11,'','Milos','Zdravo svima!','2018-01-07 06:07:23'),(21,3,'','Zdravo','Ojsa!','2018-01-08 04:54:33'),(22,21,'','aegqwtqwrtqwt','qwtqwtqwtqwtqwt','2018-01-11 12:38:35');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_flags`
--

DROP TABLE IF EXISTS `comments_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_flags` (
  `flag_id` int(11) NOT NULL AUTO_INCREMENT,
  `flag_comment_id` int(11) NOT NULL,
  `flag_reasons` varchar(1000) NOT NULL,
  PRIMARY KEY (`flag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_flags`
--

LOCK TABLES `comments_flags` WRITE;
/*!40000 ALTER TABLE `comments_flags` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments_flags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(64) NOT NULL,
  `post_content` varchar(15000) NOT NULL,
  `post_attachments` varchar(25000) NOT NULL,
  `post_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `post_desc` varchar(255) NOT NULL,
  `post_allow_comments` tinyint(1) NOT NULL,
  `post_url_slug` varchar(255) NOT NULL,
  `post_writer` varchar(100) NOT NULL,
  `post_visible` tinyint(4) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (23,'Zdravko kurvetine! sta se rad!','Zdravko kurvetine! sta se rad!Zdravko kurvetine! sta se rad!','','2018-01-13 03:12:57','qwrqwtqwtqwtqwtqwt',1,'zdravko-kurvetine-sta-se-rad-','konstantinbulatovic1@gmail.com',1),(25,'qwtqwtqwtqwt','<p>Bla bla bla bla</p><br><p>I really like this stuff!</p><p style=\"text-align: center; \">Beacause i dont think it deserves the proper repsect it should. Cheers</p><br><p style=\"text-align: right;\">Something right!</p>','','2018-01-13 04:10:32','Hello world from me',1,'qwtqwtqwtqwt','konstantinbulatovic1@gmail.com',1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `full_name` varchar(512) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `pwd_reset_token` varchar(32) DEFAULT NULL,
  `pwd_reset_token_creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_idx` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-13  7:42:09
